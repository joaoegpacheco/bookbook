import React from "react";
import "antd/dist/antd.css";
import "./Form.css";
import { Form, Input, Button, notification } from "antd";
import { withRouter, Link } from "react-router-dom";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import errorRules from "./field-validation/validator";
import PasswordStrength from "./field-validation/password-strength/PasswordStrength";

const { Password } = Input;
const { Item } = Form;

const defaultState = {
  name: "",
  user: "",
  email: "",
  password: "",
};

class Forms extends React.Component {
  state = defaultState;

  showPassword = (visible) => {
    return visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />;
  };

  setField = (e, field) => {
    this.setState({ ...this.state, [field]: e.target.value });
  };

  propsDinamicas = (field, name) => ({
    style: { height: 54 },
    placeholder: name,
    value: this.state[field].value,
    onChange: (e) => {
      this.setField(e, field);
    },
  });

  onFinishFailed = (errorInfo) => {
    return notification.error(
      {
        message: "Cadastro não realizado",
        description: "Verifique os campos e tente novamente",
      },
      4
    );
  };

  onFinish = (values) => {
    const url = "https://ka-users-api.herokuapp.com/users";
    const { name, user, email, password } = this.state;

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        user: {
          name,
          user,
          email,
          password,
          password_confirmation: password,
        },
      }),
    }).then((res) => {
      if (res.ok) {
        return this.props.history.push("/login");
      } else {
        notification.error(
          {
            message: "Usuário e/ou e-mail já cadastrados",
            description:
              "Verifique os campos de usuário e/ou e-mail e tente novamente",
          },
          4
        );
        this.setState({ user: "", email: "" });
      }
    });
  };

  render() {
    const { password } = this.state;
    return (
      <div className="FormPage">
        <Form
          layout="vertical"
          onFinish={this.onFinish}
          onFinishFailed={this.onFinishFailed}
          style={{ width: 700, padding: 70 }}
        >
          <Item label="NOME" name="username" rules={errorRules.name}>
            <Input {...this.propsDinamicas("name", "Digite seu nome")} />
          </Item>

          <Item label="USUÁRIO" name="user" rules={errorRules.user}>
            <Input {...this.propsDinamicas("user", "Digite seu usuário")} />
          </Item>

          <Item label="EMAIL" name="email" rules={errorRules.email}>
            <Input {...this.propsDinamicas("email", "Digite seu email")} />
          </Item>

          <Item label="SENHA" name="password" rules={errorRules.password}>
            <Password
              {...this.propsDinamicas("password", "Digite sua senha")}
              iconRender={this.showPassword}
            />
          </Item>
          {password && (
            <Item style={{ marginTop: "-15px" }}>
              <PasswordStrength valor={password} />
            </Item>
          )}

          <Button
            type="primary"
            size="large"
            htmlType="submit"
            style={{
              marginTop: 10,
              marginBottom: 6,
              height: 64,
              width: "100%",
              borderRadius: 4,
            }}
          >
            CADASTRAR-SE
          </Button>
          <Link to="/login">Entrar</Link>
        </Form>
      </div>
    );
  }
}

export default withRouter(Forms);
